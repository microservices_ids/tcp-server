package com.ghsoft.tcpserver.store;

import java.util.ArrayList;
import java.util.List;

public class Store {

    private final List<String> messages;

    public Store() {
        this.messages = new ArrayList<>();
    }


    /**
     * Add new message, keeps duplicated messages (validate rules)
     *
     * @param message: socket message
     */
    public String addMessage(String message) {
        String parsedMessage = this.parseMessage(message);
        boolean added = this.messages.add(parsedMessage);
        printStore();
        return added ? "ADDED" : "THE MESSAGE COULD NOT BE ADDED";
    }

    public String removeMessage(String rawMessage) {
        String parsedMessage = this.parseMessage(rawMessage);
        // Optional<String> existMessage = this.messages.stream().filter(m -> m.equals(parsedMessage)).findFirst();
        String result;
        int idx = this.messages.indexOf(parsedMessage);
        if (idx >= 0) {
            // this.messages = this.messages.stream().filter(m -> !m.equals(parsedMessage)).collect(Collectors.toList());
            String remove = this.messages.remove(idx);
            result = remove.equals(parsedMessage) ? "REMOVED" : "ERROR TRYING TO REMOVE";
        } else {
            result = "THE MESSAGE DOES NOT EXIST TO REMOVE";
        }
        printStore();
        return result;
    }

    private void printStore() {
        System.out.println("STORE DATA");
        this.messages.forEach(i -> {
            System.out.println(" - " + i);
        });
        System.out.println();
    }

    private String parseMessage(String rawMessage) {
        String[] parts = rawMessage.split(":");
        return parts[1];
    }

}
