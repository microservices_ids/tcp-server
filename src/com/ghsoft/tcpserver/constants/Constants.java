package com.ghsoft.tcpserver.constants;

public class Constants {

    public static final String serverName = "127.0.0.1";
    public static final int serverPort = 9191;

}
