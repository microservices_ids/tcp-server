package com.ghsoft.tcpserver.constants;

public enum SocketActions {

    ADD("ADD"),
    REMOVE("REMOVE");

    private final String action;

    SocketActions(String action) {
        this.action = action;
    }

    public String action() {
        return this.action;
    }
}
