package com.ghsoft.tcpserver.client;

import com.ghsoft.tcpserver.constants.Constants;
import com.ghsoft.tcpserver.constants.SocketActions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public void startConnection(String ip, int port) {
        try {
            clientSocket = new Socket(ip, port);
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        out.println(message);
        try {
            String response = in.readLine();
            System.out.println("Response: " + response + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopConnection() {
        try {
            in.close();
            out.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Client client1 = new Client();
        client1.startConnection(Constants.serverName, Constants.serverPort);
        Scanner scanner = new Scanner(System.in);

        // send 10 messages every second
//        for (int i = 1; i <= 10; i++) {
//            SocketActions randomAction = Math.random() > 0.5 ? SocketActions.ADD : SocketActions.REMOVE;
//            String message = randomAction.action() + ":Message " + i;
//            client1.sendAddMessage(message);
//            Thread.sleep(1000);
//        }

        while (true) {
            System.out.println("Select a option:");
            System.out.println("1) Add");
            System.out.println("2) Remove");
            System.out.println("3) Exit");
            System.out.print("Option: ");
            String optionAux = scanner.nextLine();
            int option = 0;
            try {
                option = Integer.parseInt(optionAux);
            } catch (Exception e) {
                System.out.println("Invalid option!\n");
                continue;
            }
            if (option >= 1 && option <= 3) {
                if (option == 3) {
                    break;
                }
                String text;
                if (option == 1) {
                    System.out.print("Message to add: ");
                    text = new Scanner(System.in).nextLine();
                    text = SocketActions.ADD.action() + ":" + text;
                } else {
                    System.out.print("Message to remove: ");
                    text = new Scanner(System.in).nextLine();
                    text = SocketActions.REMOVE.action() + ":" + text;
                }
                client1.sendMessage(text);
            } else {
                System.out.println("Invalid option!\n");
            }
        }

        client1.stopConnection();
    }
}
