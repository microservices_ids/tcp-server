package com.ghsoft.tcpserver.server;

import com.ghsoft.tcpserver.constants.Constants;
import com.ghsoft.tcpserver.store.Store;

import java.io.IOException;
import java.net.ServerSocket;

public class ServerTCP {

    private ServerSocket serverSocket;
    private final Store store;

    public ServerTCP() {
        this.store = new Store();
    }

    public void initServer(int port) {
        try {
            // create new thread by every client
            serverSocket = new ServerSocket(port);
            // Every time the while loop is executed, it blocks on accept call until a new client connects
            while (true) {
                new ClientHandler(serverSocket.accept(), this.store).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ServerTCP server = new ServerTCP();
        server.initServer(Constants.serverPort);
    }
}
