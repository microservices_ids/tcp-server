package com.ghsoft.tcpserver.server;

import com.ghsoft.tcpserver.constants.SocketActions;
import com.ghsoft.tcpserver.store.Store;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientHandler extends Thread {

    private final Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    private final Store store;

    public ClientHandler(Socket socket, Store store) {
        this.clientSocket = socket;
        this.store = store;
    }

    public void run() {
        try {
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            String message;
            while ((message = in.readLine()) != null) {
                if ("close".equals(message)) {
                    out.println("Connection closed...");
                    break;
                }
                this.handleMessage(message);
            }

            in.close();
            out.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleMessage(String message) {
        String result;
        if (message.split(":")[0].equals(SocketActions.ADD.action())) {
            result = this.store.addMessage(message);
        } else {
            result = this.store.removeMessage(message);
        }
        out.println(result); // response to the client
    }

}
